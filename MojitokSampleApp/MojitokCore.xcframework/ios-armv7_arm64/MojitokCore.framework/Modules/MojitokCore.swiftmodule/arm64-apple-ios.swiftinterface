// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target arm64-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MojitokCore
import Foundation
@_exported import MojitokCore
import StoreKit
import Swift
import SystemConfiguration
import UIKit
public struct MJTPeriod : Swift.Codable {
  public let startDate: Swift.String
  public let endDate: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct MJTPackageCuration : Swift.Codable {
  public let type: Swift.String
  public let period: MojitokCore.MJTPeriod
  public let packages: [MojitokCore.MJTPack]
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
final public class MojitokCoreManager {
  public static var isConnected: Swift.Bool {
    get
  }
  public static func setup(applicationID: Swift.String, userID: Swift.String)
  public static func connect(applicationToken: Swift.String, completion: @escaping (MojitokCore.MJTError?) -> Swift.Void, _ refresh: (() -> Swift.Void)? = nil)
  public static func registerInAppProducts(_ products: Swift.Set<MojitokCore.MJTInAppProductType>)
  @objc deinit
}
public struct MJTUserToken : Swift.Codable {
  public let mojitokUserAccessToken: Swift.String
  public let mojitokUserRefreshToken: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct MJTResponseData<T> : Swift.Codable where T : Swift.Decodable, T : Swift.Encodable {
  public let data: T
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension UIImage {
  public func getGrayScale() -> UIKit.UIImage
}
public struct MJTTag2Stickers : Swift.Codable {
  public let tag: Swift.String
  public let stickers: [MojitokCore.MJTSticker]
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public enum MJTViewType : Swift.String {
  case spotlightTab
  case ownedPackTab
  case trendingTab
  case emotionTab
  case searchTab
  case recentTab
  case rtsView
  public var name: Swift.String {
    get
  }
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
extension URL {
  public static let empty: Foundation.URL
}
final public class MJTPaymentService {
  public static let shared: MojitokCore.MJTPaymentService
  final public func setup()
  final public func purchase(type: MojitokCore.MJTProductType) -> Swift.Int?
  final public func register(_ products: Swift.Set<MojitokCore.MJTInAppProductType>)
  @objc deinit
}
public enum MJTErrorType : Swift.String, Swift.Codable {
  case api
  case connect
  case token
  case iap
  case network
  case mojitokServer
  case scheme
  case etc
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
public enum MJTErrorTarget : Swift.String, Swift.Codable {
  case app
  case sdk
  case server
  public typealias RawValue = Swift.String
  public init?(rawValue: Swift.String)
  public var rawValue: Swift.String {
    get
  }
}
final public class MJTDataClient {
  public static let `default`: MojitokCore.MJTDataClient
  final public func data(url: Foundation.URL, completion: @escaping (Foundation.Data?, MojitokCore.MJTError?) -> Swift.Void)
  @objc deinit
}
public struct MJTPack : Swift.Codable {
  public let id: Swift.String
  public let name: [MojitokCore.MJTMultiLang]
  public let description: [MojitokCore.MJTMultiLang]
  public let thumbnails: [MojitokCore.MJTImage]
  public let creator: MojitokCore.MJTCreator?
  public let stickers: [MojitokCore.MJTSticker]?
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension MJTPack {
  public var thumbnailURLString: Swift.String {
    get
  }
  public var thumbnailURL: Foundation.URL {
    get
  }
  public var defaultName: Swift.String {
    get
  }
}
public enum MJTError {
  case connect
  case applicationToken
  case networkState
  case request
  case response
  case scheme
  case server(description: Swift.String)
  case iap(description: Swift.String)
  case payment(description: Swift.String)
  case etc(description: Swift.String)
}
final public class MJTAnalyticsClient {
  public static let shared: MojitokCore.MJTAnalyticsClient
  @objc deinit
}
final public class MJTAnalyticsService {
  public static let shared: MojitokCore.MJTAnalyticsService
  final public func addStat(view: MojitokCore.MJTViewType, event: MojitokCore.MJTAnalyticsService.MJTEventType, ids: [Swift.String])
  public enum MJTEventType : Swift.String {
    case view
    case choose
    public typealias RawValue = Swift.String
    public init?(rawValue: Swift.String)
    public var rawValue: Swift.String {
      get
    }
  }
  @objc deinit
}
public struct MJTSticker : Swift.Codable {
  public let id: Swift.String
  public let images: [MojitokCore.MJTImage]
  public let packID: Swift.String?
  public let pack: MojitokCore.MJTPack?
  public let creator: MojitokCore.MJTCreator?
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension MJTSticker {
  public var thumbnailURLString: Swift.String {
    get
  }
  public var thumbnailURL: Foundation.URL {
    get
  }
  public var originalURLString: Swift.String {
    get
  }
}
public struct MJTSNS : Swift.Codable {
  public let type: Swift.String
  public let url: Swift.String
  public let priority: Swift.Int
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct MJTMultiLang : Swift.Codable {
  public let language: Swift.String
  public let content: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public enum MJTInAppProductType : Swift.Hashable {
  case stickerPack(productID: Swift.String)
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: MojitokCore.MJTInAppProductType, b: MojitokCore.MJTInAppProductType) -> Swift.Bool
}
public enum MJTProductType : Swift.Hashable {
  case stickerPack(objectID: Swift.String)
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
  public static func == (a: MojitokCore.MJTProductType, b: MojitokCore.MJTProductType) -> Swift.Bool
}
final public class FilesManager {
  public static let rootDirectoryURL: Foundation.URL
  public static let mojitokUIKitFolderURL: Foundation.URL
  public static let cacheFolderURL: Foundation.URL
  public static func saveImage(url: Foundation.URL, data: Foundation.Data)
  public static func dataToFile(at url: Foundation.URL, data: Foundation.Data)
  public static func createDirectory(at url: Foundation.URL)
  public static func existDirectory(at url: Foundation.URL) -> Swift.Bool
  public static func checkDirectory(url: Foundation.URL)
  @objc deinit
}
public struct MJTOwnership : Swift.Codable {
  public let packID: Swift.String
  public let ownType: Swift.String
  public let acquireDate: Swift.String
  public let dueDate: Swift.String?
  public let pack: MojitokCore.MJTPack?
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public struct MJTImage : Swift.Codable {
  public let format: Swift.String
  public let spec: Swift.String
  public let url: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
final public class MJTContentsClient {
  public static let shared: MojitokCore.MJTContentsClient
  final public func trending(includes: [Swift.String] = [], completion: @escaping ([MojitokCore.MJTSticker], MojitokCore.MJTError?) -> Swift.Void)
  final public func featured(types: [Swift.String], completion: @escaping ([MojitokCore.MJTPackageCuration], MojitokCore.MJTError?) -> Swift.Void)
  final public func freePacks(skip: Swift.Int = 0, limit: Swift.Int = 20, includes: [Swift.String] = [], completion: @escaping ([MojitokCore.MJTPack], MojitokCore.MJTError?) -> Swift.Void)
  final public func keywordsTags(type: Swift.String = "TOP_20", completion: @escaping ([Swift.String], MojitokCore.MJTError?) -> Swift.Void)
  final public func emotionTag2Stickers(includes: [Swift.String] = [], completion: @escaping ([MojitokCore.MJTTag2Stickers], MojitokCore.MJTError?) -> Swift.Void)
  final public func emojiTag2Stickers(includes: [Swift.String] = [], completion: @escaping ([MojitokCore.MJTTag2Stickers], MojitokCore.MJTError?) -> Swift.Void)
  final public func ownerShip(include: Swift.String = "STICKER_PACK", skip: Swift.Int = 0, limit: Swift.Int = 20, completion: @escaping ([MojitokCore.MJTOwnership], MojitokCore.MJTError?) -> Swift.Void)
  @objc deinit
}
final public class MJTAuthService {
  public static let tokenFail: Foundation.Notification.Name
  public static let tokenUpdate: Foundation.Notification.Name
  public static let shared: MojitokCore.MJTAuthService
  final public func setup(applicationID: Swift.String, userID: Swift.String)
  final public func connect(applicationToken: Swift.String, completion: @escaping (MojitokCore.MJTError?) -> Swift.Void, refresh: (() -> Swift.Void)?)
  final public func getLatestToken() -> MojitokCore.MJTUserToken?
  final public func getUserID() -> Swift.String?
  @objc deinit
}
public enum MJTSDK {
  public static var isOldDevice: Swift.Bool {
    get
  }
}
public struct MJTCreator : Swift.Codable {
  public let id: Swift.String
  public let nickname: [MojitokCore.MJTMultiLang]
  public let SNSs: [MojitokCore.MJTSNS]
  public let stickers: [MojitokCore.MJTSticker]?
  public let packs: [MojitokCore.MJTPack]?
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension MJTCreator {
  public var defaultNickname: Swift.String {
    get
  }
  public var defaultSNS: Swift.String? {
    get
  }
}
extension MojitokCore.MJTViewType : Swift.Equatable {}
extension MojitokCore.MJTViewType : Swift.Hashable {}
extension MojitokCore.MJTViewType : Swift.RawRepresentable {}
extension MojitokCore.MJTErrorType : Swift.Equatable {}
extension MojitokCore.MJTErrorType : Swift.Hashable {}
extension MojitokCore.MJTErrorType : Swift.RawRepresentable {}
extension MojitokCore.MJTErrorTarget : Swift.Equatable {}
extension MojitokCore.MJTErrorTarget : Swift.Hashable {}
extension MojitokCore.MJTErrorTarget : Swift.RawRepresentable {}
extension MojitokCore.MJTAnalyticsService.MJTEventType : Swift.Equatable {}
extension MojitokCore.MJTAnalyticsService.MJTEventType : Swift.Hashable {}
extension MojitokCore.MJTAnalyticsService.MJTEventType : Swift.RawRepresentable {}
