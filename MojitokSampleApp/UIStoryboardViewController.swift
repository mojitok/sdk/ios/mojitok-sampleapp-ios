//
//  StoryboardBaseViewController.swift
//  MojitokUIKitSampleApp
//
//  Created by GAMZA on 2021/02/05.
//

import UIKit
import MojitokUIKit

class StoryboardBaseViewController: UIViewController {
    
    // UIKit
    @IBOutlet weak var textField: UITextField!
    
    // MojitokUIKit
    @IBOutlet weak var mjtMainViewArea: UIView!
    @IBOutlet weak var mjtRTSViewArea: UIView!
    @IBOutlet weak var imageView: MJTImageView!
    
    let mjtMainView = MJTMainView()
    let mjtRTSView = MJTRTSView()
    
    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: MJTMainView Setup
        mjtMainViewArea.addSubview(mjtMainView)
        mjtMainView.delegate = self
        
        // MARK: MJTRTSView Setup
        mjtRTSViewArea.addSubview(mjtRTSView)
        mjtRTSView.delegate = self
        
        setupRTS()
        
        navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        mjtMainView.frame = mjtMainViewArea.bounds
        mjtRTSView.frame = mjtRTSViewArea.bounds
    }
    
    private func setupRTS() {
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: textField)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        if let textField = notification.object as? UITextField {
            if let keyword = self.textField.text,
               keyword != "" {
                self.mjtRTSView.searchAndUpdate(keyword: textField.text ?? "")
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension StoryboardBaseViewController: MJTMainViewDelegate, MJTRTSViewDelegate {
    func didSelectedSticker(mjtStickerInfo: MJTStickerInfo) {
        print("Select URL: \(mjtStickerInfo.url)")
        imageView.setMojitokSticker(with: mjtStickerInfo.url)
    }
}
