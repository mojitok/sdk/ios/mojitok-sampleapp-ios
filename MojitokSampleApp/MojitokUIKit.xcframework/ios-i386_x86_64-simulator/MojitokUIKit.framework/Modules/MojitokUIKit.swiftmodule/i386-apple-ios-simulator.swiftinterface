// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
// swift-module-flags: -target i386-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name MojitokUIKit
import CoreGraphics
import Foundation
import ImageIO
import MobileCoreServices
import MojitokCore
@_exported import MojitokUIKit
import SQLite3
import Swift
import UIKit
import WebKit
public enum MJTEditMode {
  case sort
  case hide
  public static func == (a: MojitokUIKit.MJTEditMode, b: MojitokUIKit.MJTEditMode) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
public struct MJTStickerInfo : Swift.Codable {
  public let url: Swift.String
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol MJTMainViewDelegate : AnyObject {
  func didSelectedSticker(mjtStickerInfo: MojitokUIKit.MJTStickerInfo)
}
@objc @IBDesignable final public class MJTMainView : UIKit.UIView {
  weak final public var delegate: MojitokUIKit.MJTMainViewDelegate?
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public init(frame: CoreGraphics.CGRect = .zero)
  final public func commonInit()
  @objc override final public func layoutSubviews()
  @objc deinit
}
extension MJTMainView : UIKit.UICollectionViewDataSource {
  @objc final public func numberOfSections(in collectionView: UIKit.UICollectionView) -> Swift.Int
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, numberOfItemsInSection section: Swift.Int) -> Swift.Int
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, cellForItemAt indexPath: Foundation.IndexPath) -> UIKit.UICollectionViewCell
}
extension MJTMainView : UIKit.UICollectionViewDataSourcePrefetching {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, prefetchItemsAt indexPaths: [Foundation.IndexPath])
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, cancelPrefetchingForItemsAt indexPaths: [Foundation.IndexPath])
}
extension MJTMainView : UIKit.UICollectionViewDelegate {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, willDisplay cell: UIKit.UICollectionViewCell, forItemAt indexPath: Foundation.IndexPath)
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, didEndDisplaying cell: UIKit.UICollectionViewCell, forItemAt indexPath: Foundation.IndexPath)
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, didSelectItemAt indexPath: Foundation.IndexPath)
}
extension MJTMainView : UIKit.UICollectionViewDelegateFlowLayout {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, sizeForItemAt indexPath: Foundation.IndexPath) -> CoreGraphics.CGSize
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Swift.Int) -> CoreGraphics.CGFloat
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, minimumLineSpacingForSectionAt section: Swift.Int) -> CoreGraphics.CGFloat
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, insetForSectionAt section: Swift.Int) -> UIKit.UIEdgeInsets
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, viewForSupplementaryElementOfKind kind: Swift.String, at indexPath: Foundation.IndexPath) -> UIKit.UICollectionReusableView
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, referenceSizeForHeaderInSection section: Swift.Int) -> CoreGraphics.CGSize
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, referenceSizeForFooterInSection section: Swift.Int) -> CoreGraphics.CGSize
}
final public class MJTUIManager {
  public static let shared: MojitokUIKit.MJTUIManager
  public enum MJTUILanguage {
    case korean
    case english
    case indoneisan
    public static func == (a: MojitokUIKit.MJTUIManager.MJTUILanguage, b: MojitokUIKit.MJTUIManager.MJTUILanguage) -> Swift.Bool
    public var hashValue: Swift.Int {
      get
    }
    public func hash(into hasher: inout Swift.Hasher)
  }
  final public func setRTSView(isHiden: Swift.Bool)
  final public func setup(diskCacheSize: Swift.Int? = nil, mainColor: UIKit.UIColor? = nil, language: MojitokUIKit.MJTUIManager.MJTUILanguage? = nil, theme: MojitokUIKit.MJTColorTheme? = nil)
  @objc deinit
}
public protocol MJTRTSViewDelegate : AnyObject {
  func didSelectedSticker(mjtStickerInfo: MojitokUIKit.MJTStickerInfo)
}
@objc @IBDesignable final public class MJTRTSView : UIKit.UIView {
  public enum MJTRTSOption {
    case throttle(time: Swift.Int)
    case whiteSpace(count: Swift.Int)
    case change(count: Swift.Int)
  }
  weak final public var delegate: MojitokUIKit.MJTRTSViewDelegate?
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  public init(frame: CoreGraphics.CGRect, options: [MojitokUIKit.MJTRTSView.MJTRTSOption] = [])
  @objc override dynamic public init(frame: CoreGraphics.CGRect = .zero)
  final public func searchAndUpdate(keyword: Swift.String)
  @objc deinit
}
extension MJTRTSView : UIKit.UICollectionViewDataSourcePrefetching {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, prefetchItemsAt indexPaths: [Foundation.IndexPath])
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, cancelPrefetchingForItemsAt indexPaths: [Foundation.IndexPath])
}
extension MJTRTSView : UIKit.UICollectionViewDataSource {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, numberOfItemsInSection section: Swift.Int) -> Swift.Int
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, cellForItemAt indexPath: Foundation.IndexPath) -> UIKit.UICollectionViewCell
}
extension MJTRTSView : UIKit.UICollectionViewDelegate {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, willDisplay cell: UIKit.UICollectionViewCell, forItemAt indexPath: Foundation.IndexPath)
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, didEndDisplaying cell: UIKit.UICollectionViewCell, forItemAt indexPath: Foundation.IndexPath)
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, didSelectItemAt indexPath: Foundation.IndexPath)
}
extension MJTRTSView : UIKit.UICollectionViewDelegateFlowLayout {
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, sizeForItemAt indexPath: Foundation.IndexPath) -> CoreGraphics.CGSize
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Swift.Int) -> CoreGraphics.CGFloat
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, minimumLineSpacingForSectionAt section: Swift.Int) -> CoreGraphics.CGFloat
  @objc final public func collectionView(_ collectionView: UIKit.UICollectionView, layout collectionViewLayout: UIKit.UICollectionViewLayout, insetForSectionAt section: Swift.Int) -> UIKit.UIEdgeInsets
}
public class MTTypography {
  @objc deinit
}
extension UIImage {
  public func webpRepresentation(isLossy: Swift.Bool = false, quality: Swift.Float = 75.0) -> Foundation.Data?
}
extension UIImage {
  public static func image(webpData: Foundation.Data, scale: CoreGraphics.CGFloat, onlyFirstFrame: Swift.Bool) -> UIKit.UIImage?
}
extension Data {
  public var isWebPFormat: Swift.Bool {
    get
  }
}
@objc final public class MJTImageView : UIKit.UIImageView {
  @objc override final public func display(_ layer: QuartzCore.CALayer)
  final public func setMojitokSticker(with urlString: Swift.String)
  @objc override dynamic public init(image: UIKit.UIImage?)
  @available(iOS 3.0, *)
  @objc override dynamic public init(image: UIKit.UIImage?, highlightedImage: UIKit.UIImage?)
  @objc override dynamic public init(frame: CoreGraphics.CGRect)
  @objc required dynamic public init?(coder: Foundation.NSCoder)
  @objc deinit
}
final public class Mojitok {
  public static func setup(applicationID: Swift.String, userID: Swift.String)
  public static func connect(applicationToken: Swift.String, completion: @escaping (MojitokCore.MJTError?) -> Swift.Void, _ refresh: (() -> Swift.Void)? = nil)
  public static func registerInAppProducts(_ products: Swift.Set<MojitokCore.MJTInAppProductType>)
  @objc deinit
}
public enum MJTColorTheme {
  case light
  case dark
  public static func == (a: MojitokUIKit.MJTColorTheme, b: MojitokUIKit.MJTColorTheme) -> Swift.Bool
  public var hashValue: Swift.Int {
    get
  }
  public func hash(into hasher: inout Swift.Hasher)
}
extension MojitokUIKit.MJTEditMode : Swift.Equatable {}
extension MojitokUIKit.MJTEditMode : Swift.Hashable {}
extension MojitokUIKit.MJTUIManager.MJTUILanguage : Swift.Equatable {}
extension MojitokUIKit.MJTUIManager.MJTUILanguage : Swift.Hashable {}
extension MojitokUIKit.MJTColorTheme : Swift.Equatable {}
extension MojitokUIKit.MJTColorTheme : Swift.Hashable {}
