//
//  AppDelegate.swift
//  MojitokSampleApp
//
//  Created by 김진우 on 2021/01/05.
//

import UIKit
import MojitokUIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Mojitok.setup(applicationID: "c7ddb150-4a6c-4b65-b5f9-22125b0dd92b", userID: "Mango2")
        
        Mojitok.connect(applicationToken: "qwertyui12345678qwertyui12345678", completion: { error in
            if let err = error {
                print(err)
            }
        })
        
        Mojitok.registerInAppProducts([.stickerPack(productID: "COM.MOJITOK.SDK.Test")])
        
        // MARK: - MojitokUIKit Setup
        MJTUIManager.shared.setup(diskCacheSize: 1000 * 1000 * 1, mainColor: .blue, language: .english, theme: .light)
        
        return true
    }
}

