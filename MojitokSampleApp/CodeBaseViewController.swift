//
//  CodeBaseViewController.swift
//  MojitokUIKitSampleApp
//
//  Created by GAMZA on 2021/02/05.
//

import UIKit
import MojitokUIKit

final class CodeBaseViewController: UIViewController {
    
//     MARK: - MojitokUIKit Component
    lazy var mjtMainView: MJTMainView = {
        let mjtMainView = MJTMainView()
        self.view.addSubview(mjtMainView)
        mjtMainView.translatesAutoresizingMaskIntoConstraints = false
        mjtMainView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        mjtMainView.heightAnchor.constraint(equalTo: mjtMainView.widthAnchor, multiplier: 0.688).isActive = true
        mjtMainView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        if #available(iOS 11.0, *) {
            mjtMainView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        } else {
            mjtMainView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        }
        mjtMainView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        return mjtMainView
    }()

    lazy var mjtRTSView: MJTRTSView = {
        let mjtRTSView = MJTRTSView()
        self.view.addSubview(mjtRTSView)
        mjtRTSView.translatesAutoresizingMaskIntoConstraints = false
        mjtRTSView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        mjtRTSView.bottomAnchor.constraint(equalTo: searchBackgroundView.topAnchor).isActive = true
        mjtRTSView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        mjtRTSView.heightAnchor.constraint(equalToConstant: 96).isActive = true
        return mjtRTSView
    }()

    // MARK: - Support Component
    lazy var imageView: MJTImageView = {
        let imageView = MJTImageView(frame: .zero)
        self.view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor).isActive = true
        if #available(iOS 11.0, *) {
            imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        } else {
            imageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        }
        imageView.bottomAnchor.constraint(equalTo: mjtRTSView.topAnchor).isActive = true
        return imageView
    }()

    lazy var searchBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 0.36, green: 0.36, blue: 0.36, alpha: 0.3)
        self.view.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.mjtMainView.topAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.heightAnchor.constraint(equalToConstant: 30).isActive = true
        return view
    }()

    lazy var labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 3
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(lineStackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.heightAnchor.constraint(equalToConstant: 36).isActive
         = true
        return stackView
    }()

    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "RTSTest"
        label.widthAnchor.constraint(equalToConstant: 50).isActive = true
        label.font = .systemFont(ofSize: 12)
        return label
    }()

    lazy var lineStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 0
        stackView.addArrangedSubview(iconStackView)
        stackView.addArrangedSubview(line)
        return stackView
    }()

    lazy var line: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return view
    }()

    lazy var iconStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 0
        stackView.addArrangedSubview(searchIconImageView)
        stackView.addArrangedSubview(textField)
        return stackView
    }()

    lazy var textField: UITextField = {
        let textField = UITextField()
        textField.borderStyle = .none
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.heightAnchor.constraint(equalToConstant: 24).isActive = true
        return textField
    }()

    lazy var searchIconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "search")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.widthAnchor.constraint(equalToConstant: 24).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 24).isActive = true
        return imageView
    }()

    // MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: MJTMainView Setup
        mjtMainView.delegate = self
        
        // MARK: MJTRTSView Setup
        mjtRTSView.delegate = self
        setupRTS()
        
        self.addSearchBar()
        
        navigationController?.navigationBar.isHidden = true
    }

    private func addSearchBar() {
        searchBackgroundView.addSubview(labelStackView)
        labelStackView.leadingAnchor.constraint(equalTo: searchBackgroundView.leadingAnchor, constant: 12).isActive = true
        labelStackView.trailingAnchor.constraint(equalTo: searchBackgroundView.trailingAnchor, constant: -12).isActive = true
        labelStackView.centerYAnchor.constraint(equalTo: searchBackgroundView.centerYAnchor).isActive = true
    }

    private func setupRTS() {
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: textField)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        if let textField = notification.object as? UITextField {
            if let keyword = self.textField.text,
               keyword != "" {
                self.mjtRTSView.searchAndUpdate(keyword: textField.text ?? "")
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}

extension CodeBaseViewController: MJTMainViewDelegate, MJTRTSViewDelegate {
    func didSelectedSticker(mjtStickerInfo: MJTStickerInfo) {
        print("Select URL: \(mjtStickerInfo.url)")
        imageView.setMojitokSticker(with: mjtStickerInfo.url)
    }
}
