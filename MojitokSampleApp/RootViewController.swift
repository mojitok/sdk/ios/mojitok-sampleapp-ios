//
//  RootViewController.swift
//  MojitokSampleApp
//
//  Created by GAMZA on 2021/02/10.
//

import UIKit
import MojitokUIKit

final class RootViewController: UIViewController {
    
    @IBAction func rtsTogleSwitchAction(_ sender: UISwitch) {
        MJTUIManager.shared.setRTSView(isHiden: !sender.isOn)
    }
    
    @IBAction func themeModeSwitchAction(_ sender: UISwitch) {
        MJTUIManager.shared.setup(theme: sender.isOn ? .dark : .light)
    }
    
    @IBAction func languageSwitchAction(_ sender: UISwitch) {
        MJTUIManager.shared.setup(language: sender.isOn ? .indoneisan : .english)
    }
    
    @IBAction func mainColorSwitchAction(_ sender: UISwitch) {
        MJTUIManager.shared.setup(mainColor: sender.isOn ? UIColor(red: 229/255, green: 47/255, blue: 23/255, alpha: 1) : .blue)
    }
}
